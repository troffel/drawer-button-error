package com.example.testdrawer;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;


public class PrefFragment extends Fragment{
    
    private Context context;
    private View checkbox_container;
    private Button settingsBack;

    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.preffragment, container, false);
        checkbox_container = v;
        settingsBack = (Button) v.findViewById(R.id.settings_back);
        settingsBack.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                saveSettings();
            }
        });
        
        //setupSimplePreferencesScreen();
        
        return v;

    }
    
    public void saveSettings(){
        //setSettings(checkbox_container);
        
        FragmentManager fmanager = getFragmentManager();
        FragmentTransaction ftrans = fmanager.beginTransaction();
        ftrans.replace(R.id.left_drawer, new CategoryFragment());
        ftrans.commit();
        
        Log.i("test", "saving settings");
    }
}

package com.example.testdrawer;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

public class MainActivity extends ActionBarActivity {

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        
        fragmentTransaction.add(R.id.left_drawer, new CategoryFragment());
        fragmentTransaction.commit();
    }
    
    public void openSettings(View v){
        FragmentManager fmanager = getSupportFragmentManager();
        FragmentTransaction ftrans = fmanager.beginTransaction();
        PrefFragment prefFragment = new PrefFragment();
        ftrans.replace(R.id.left_drawer, prefFragment);
        ftrans.commit();
        Log.i("test", "Opening settings");
    }
}
